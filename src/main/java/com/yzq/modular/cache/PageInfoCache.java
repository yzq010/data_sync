package com.yzq.modular.cache;

import com.yzq.modular.core.PageInfo;
import com.yzq.modular.core.ProjectConstants;
import com.yzq.modular.core.AbstractRedisCacheOperator;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/16 19:40
 */
public class PageInfoCache extends AbstractRedisCacheOperator<PageInfo> {

    public PageInfoCache(RedisTemplate<String, PageInfo> redisTemplate) {
        super(redisTemplate);
    }

    @Override
    public String getCommonKeyPrefix() {
        return ProjectConstants.PAGEINFO_CACHE_PREFIX;
    }

    /*@Override
    public String calcKey(String keyParam) {
        return null;
    }*/
}
