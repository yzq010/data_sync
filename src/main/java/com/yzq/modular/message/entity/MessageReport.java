package com.yzq.modular.message.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

import static com.baomidou.mybatisplus.annotation.SqlCondition.EQUAL;

/**
 * 周期上报消息记录对象 message_report
 *
 * @author jihan
 * @date 2024-05-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("message_report")
public class MessageReport  implements Serializable {
    private static final long serialVersionUID = 5884680514111267324L;
    /**
     * 设备token，设备注册服务时生成，长度小于64（奇成设备使用）
     */
    @TableField(value = "token", condition = EQUAL)
    private String token;

    /**
     * 消息ID
     */
    @TableField(value = "message_id", condition = EQUAL)
    private String messageId;

    /**
     * 租户ID
     */
    @TableField(value = "tenant_id", condition = EQUAL)
    private Long tenantId;

    /**
     * 时间戳
     */
    @TableField(value = "timestamp_value", condition = EQUAL)
    private String timestampValue;

    /**
     * 操作类型，请求：req，响应：rsp
     */
    @TableField(value = "operation_type", condition = EQUAL)
    private String operationType;

    /**
     * 终端厂商识别号
     */
    @TableField(value = "mfrs_key", condition = EQUAL)
    private String mfrsKey;

    /**
     * sn码
     */
    @TableField(value = "sn", condition = EQUAL)
    private String sn;

    /**
     * 设备IMEI，可通过此参数确认支持几张虚 卡，不支持的虚卡不允许切卡。 Eg：sim_num=1,表示 esim 参数只 支持 1 和 2，则不允 许下发 3,4 进行切卡
     */
    @TableField(value = "imei", condition = EQUAL)
    private String imei;

    /**
     * 设备 imei 号，可通过此参数确认支持几张虚 卡，不支持的虚卡不允许切卡。 Eg：sim_num=1,表示 esim 参数只 支持 1 和 2，则不允 许下发 3,4 进行切卡
     */
    @TableField(value = "sim_num", condition = EQUAL)
    private String simNum;

    /**
     * iccid
     */
    @TableField(value = "iccid", condition = EQUAL)
    private String iccid;

    /**
     * mac 地址，例子：11:22:33:44:AA:BB
     */
    @TableField(value = "mac", condition = EQUAL)
    private String mac;

    /**
     * 电量百分比，Ufi 不支持电量查询，为空
     */
    @TableField(value = "remain", condition = EQUAL)
    private String remain;

    /**
     * 上次总流量，上次上报时设备已使用流量，单位KB
     */
    @TableField(value = "amound_start", condition = EQUAL)
    private String amoundStart;

    /**
     * 当前总流量，本次上报时设备已使用流量，单 位 KB
     */
    @TableField(value = "amound_end", condition = EQUAL)
    private String amoundEnd;

    /**
     * 当前时间，设备当前时间(24 小时制)，年月日时分秒
     */
    @TableField(value = "current_tstringime", condition = EQUAL)
    private String currentTstringime;

    /**
     * 当前日期，年月日
     */
    @TableField(value = "current_date_value", condition = EQUAL)
    private String currentDateValue;

    /**
     * 热点名称，wifi 名称，使用 urlencode 编码
     */
    @TableField(value = "ssid", condition = EQUAL)
    private String ssid;

    /**
     * 热点密码
     */
    @TableField(value = "key_value", condition = EQUAL)
    private String keyValue;

    /**
     * 热点显示，WIFI 是否隐藏，0 显示 1 隐藏
     */
    @TableField(value = "hidden", condition = EQUAL)
    private String hidden;

    /**
     * 热点连接数，当前 WIFI 连接数
     */
    @TableField(value = "conn_cnt", condition = EQUAL)
    private String connCnt;

    /**
     * 信号强度，信号强度，单位 dBm
     */
    @TableField(value = "rssi", condition = EQUAL)
    private String rssi;

    /**
     * 运营商名称，CMCC（中国移动），CUCC（中国联通），CTCC（中国电信），UnKnown（未知的）
     */
    @TableField(value = "opname", condition = EQUAL)
    private String opname;

    /**
     * 日流量，当前使用 SIM 卡当天流量（KB）
     */
    @TableField(value = "dayflow", condition = EQUAL)
    private String dayflow;

    /**
     * 月流量，当前使用 SIM 卡当月流程（KB）
     */
    @TableField(value = "monthflow", condition = EQUAL)
    private String monthflow;

    /**
     * lac，位置区域码，十进制
     */
    @TableField(value = "lac", condition = EQUAL)
    private String lac;

    /**
     * cellid，Cell Id 基站编号，十进制
     */
    @TableField(value = "cid", condition = EQUAL)
    private String cid;

    /**
     * plmn，格式：XXX（mnc+mcc）
     */
    @TableField(value = "plmn", condition = EQUAL)
    private String plmn;

    /**
     * 地址，设备当前所处的地理位置描述
     */
    @TableField(value = "address", condition = EQUAL)
    private String address;

    /**
     * ip 地址，设备内部网关地址
     */
    @TableField(value = "gateway_ipaddr", condition = EQUAL)
    private String gatewayIpaddr;

    /**
     * 默认版本号，默认与 webui 显示版本号 一致
     */
    @TableField(value = "version", condition = EQUAL)
    private String version;

    /**
     * 定制版本号
     */
    @TableField(value = "sw_version", condition = EQUAL)
    private String swVersion;

    /**
     * 切卡结果，0:切卡失败,切换回原网络，1:切换并且注册上网络，2:正常状态，3:开机第一次上报
     */
    @TableField(value = "switch_resultc", condition = EQUAL)
    private String switchResultc;

    /**
     * 限速档位，0：不限速，1：表示断开网络（仅允许设备跟服务 器通讯），2：限速 2Mbps，3：限速 10Mbps
     */
    @TableField(value = "limitspeed", condition = EQUAL)
    private String limitspeed;

    /**
     * 多 esim 日流量，所有 ESIM 卡当天流量（KB）
     */
    @TableField(value = "dayflow_esim", condition = EQUAL)
    private String dayflowEsim;

    /**
     * 多 esim 月流量，所有 ESIM 卡当月流程（KB）
     */
    @TableField(value = "monthflow_esim", condition = EQUAL)
    private String monthflowEsim;

    /**
     * 多 esim 的 iccid，所有 esim 的 iccid:根据 sim_num 确定
     */
    @TableField(value = "iccid_esim", condition = EQUAL)
    private String iccidEsim;

    /**
     * 所有接入 wifi 列表
     */
    @TableField(value = "stat_list", condition = EQUAL)
    private String statList;

    /**
     * 上报数据的序列号，序列号从 0 开始，每次增 加 1， 重启后从 0 开始
     */
    @TableField(value = "serial_num", condition = EQUAL)
    private String serialNum;

    /**
     * 上报开机时间，单位秒
     */
    @TableField(value = "boot_time", condition = EQUAL)
    private String bootTime;

    /**
     * 上报短信体，上报内容为发件人+接收时间戳+ 短信内容；短信内容为 Unicode 格式字符串
     */
    @TableField(value = "sms_body", condition = EQUAL)
    private String smsBody;

    /**
     * 实卡 iccid
     */
    @TableField(value = "iccid_slot", condition = EQUAL)
    private String iccidSlot;

    /**
     * 设备接入管理（黑白名单），0:不做限制，1:白名单，2:黑名单
     */
    @TableField(value = "maclimit_mode", condition = EQUAL)
    private String maclimitMode;

    /**
     * 设备黑名单，列表为设备 MAC 地址
     */
    @TableField(value = "black_list", condition = EQUAL)
    private String blackList;

    /**
     * 设备白名单，列表为设备 MAC 地址，当 maclimit_mode=1 时为白名单 模式
     */
    @TableField(value = "white_list", condition = EQUAL)
    private String whiteList;

    /**
     * 已连接设备信息
     */
    @TableField(value = "devconn_info", condition = EQUAL)
    private String devconnInfo;

    /**
     * wifi 加密模式，0：OPEN，1：WPA2(AES)-PSK，2：WPA-PSK/WPA2PSK
     */
    @TableField(value = "wifi_encrypt", condition = EQUAL)
    private String wifiEncrypt;

    /**
     * 短信是否已满，0：未满，1：已满
     */
    @TableField(value = "sms_full", condition = EQUAL)
    private String smsFull;

    /**
     * 当前限速值(Kb)，0：限速未开启，X: 限速 X kbps
     */
    @TableField(value = "limitspeednum", condition = EQUAL)
    private String limitspeednum;

    /**
     * 主题发布日期，年月日时分秒
     */
    @TableField(value = "report_time", condition = EQUAL)
    private LocalDateTime reportTime;

    /**
     * 是否刪除(0-否；1-是)
     */
    @TableField(value = "del_flag", condition = EQUAL)
    private Boolean delFlag;

    /**
     * 设备第一张卡的iccid
     */
    @TableField(value = "iccid_first", condition = EQUAL)
    private String iccidFirst;

    /**
     * 设备第一张卡的流量值，单位：kb
     */
    @TableField(value = "iccid_first_flow", condition = EQUAL)
    private Integer iccidFirstFlow;

    /**
     * 设备第二张卡的iccid
     */
    @TableField(value = "iccid_second", condition = EQUAL)
    private String iccidSecond;

    /**
     * 设备第二张卡的流量值，单位：kb
     */
    @TableField(value = "iccid_second_flow", condition = EQUAL)
    private Integer iccidSecondFlow;

    /**
     * 设备第三张卡的iccid
     */
    @TableField(value = "iccid_third", condition = EQUAL)
    private String iccidThird;

    /**
     * 设备第三张卡的流量值，单位：kb
     */
    @TableField(value = "iccid_third_flow", condition = EQUAL)
    private Integer iccidThirdFlow;

    /**
     * 消息内容，JSON格式
     */
    @TableField(value = "message_content", condition = EQUAL)
    private String messageContent;

    /**
     * 软件版本号
     */
    @TableField(value = "software_version", condition = EQUAL)
    private String softwareVersion;

    /**
     * 设备wifi解密标识
     */
    @TableField(value = "decrypt_flag", condition = EQUAL)
    private String decryptFlag;

    /**
     * WIFI频率切换，0：切换为2.4G，1：切换为5G
     */
    @TableField(value = "wifi_freq_switch", condition = EQUAL)
    private String wifiFreqSwitch;

    /**
     * 自动关机功能，0：不自动关机，1：自动关机
     */
    @TableField(value = "auto_shutdown", condition = EQUAL)
    private String autoShutdown;

    /**
     * 双频设备频段1热点显示，wifi是否隐藏 0-显示 1- 隐藏
     */
    @TableField(value = "hidden_first_frequency", condition = EQUAL)
    private String hiddenFirstFrequency;

    /**
     * 双频设备频段2热点显示，wifi是否隐藏 0-显示 1- 隐藏
     */
    @TableField(value = "hidden_second_frequency", condition = EQUAL)
    private String hiddenSecondFrequency;

    /**
     * 双频设备频段1热点名称
     */
    @TableField(value = "ssid_first_frequency", condition = EQUAL)
    private String ssidFirstFrequency;

    /**
     * 双频设备频段2热点名称
     */
    @TableField(value = "ssid_second_frequency", condition = EQUAL)
    private String ssidSecondFrequency;

    /**
     * 双频设备频段1wifi加密模式 0-OPEN 1-WPA2(AES)-PSK 2-WPA-PSK/WPA2PSK
     */
    @TableField(value = "wifi_encrypt_first_frequency", condition = EQUAL)
    private String wifiEncryptFirstFrequency;

    /**
     * 双频设备频段2wifi加密模式 0-OPEN 1-WPA2(AES)-PSK 2-WPA-PSK/WPA2PSK
     */
    @TableField(value = "wifi_encrypt_second_frequency", condition = EQUAL)
    private String wifiEncryptSecondFrequency;

    /**
     * 双频设备频段1热点密码
     */
    @TableField(value = "key_first_frequency", condition = EQUAL)
    private String keyFirstFrequency;

    /**
     * 双频设备频段2热点密码
     */
    @TableField(value = "key_second_frequency", condition = EQUAL)
    private String keySecondFrequency;

    /**
     * 双频设备频段1wifi频段 0-切换为2.4G 1-切换为5G
     */
    @TableField(value = "with_freq_switch_first_frequency", condition = EQUAL)
    private String withFreqSwitchFirstFrequency;

    /**
     * 双频设备频段2wifi频段 0-切换为2.4G 1-切换为5G
     */
    @TableField(value = "with_freq_switch_second_frequency", condition = EQUAL)
    private String withFreqSwitchSecondFrequency;

    /**
     * 设备双频段和单频段标识 0-单频道工作 1-双频段同时工作
     */
    @TableField(value = "frequency_flag", condition = EQUAL)
    private String frequencyFlag;

    /**
     * 双频段频段1热点连接数
     */
    @TableField(value = "conn_cnt_first_frequency", condition = EQUAL)
    private Integer connCntFirstFrequency;

    /**
     * 双频段频段2热点连接数
     */
    @TableField(value = "conn_cnt_second_frequency", condition = EQUAL)
    private Integer connCntSecondFrequency;

    /**
     * 智能选网开关标识，0：开，1：关
     */
    @TableField(value = "set_switch_mode", condition = EQUAL)
    private String setSwitchMode;
    @TableField(value = "updated_time")
    private LocalDateTime updatedTime;
    @TableField(value = "updated_by")
    private String updatedBy;
    @TableId(
            value = "id",
            type = IdType.INPUT)
    private  String id;
    @TableField(
            value = "created_time"
    )
    private LocalDateTime createdTime;
    @TableField(value = "created_by")
    private String createdBy;
}
