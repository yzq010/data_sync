package com.yzq.modular.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yzq.modular.message.entity.MessageReport1;


import java.util.List;


/**
 * 周期上报消息记录Service接口
 *
 * @author jihan
 * @date 2024-05-13
 */
public interface IMessageReport1Service extends IService<MessageReport1> {

    /**
     * 插入消息报告
     *
     * @param messageReport1s 消息报告1s
     * @author yzq
     * @date 2024/10/26
     */
    void insertMessageReports(List<MessageReport1> messageReport1s);

}
