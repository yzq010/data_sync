package com.yzq.modular.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yzq.modular.message.entity.MessageReport;


/**
 * 周期上报消息记录Service接口
 *
 * @author jihan
 * @date 2024-05-13
 */
public interface IMessageReportService extends IService<MessageReport> {
    public Integer getTableTotal();

}
