package com.yzq.modular.message.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzq.modular.message.entity.MessageReport;
import com.yzq.modular.message.entity.MessageReport1;
import com.yzq.modular.message.mapper.MessageReport1Mapper;
import com.yzq.modular.message.mapper.MessageReportMapper;
import com.yzq.modular.message.service.IMessageReport1Service;
import com.yzq.modular.message.service.IMessageReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 周期上报消息记录Service业务层处理
 *
 * @author jihan
 * @date 2024-05-13
 */
@Slf4j
@Service
@DS("master_1")
public class MessageReport1ServiceImpl extends ServiceImpl<MessageReport1Mapper, MessageReport1> implements IMessageReport1Service {

    @Override
    public void insertMessageReports(List<MessageReport1> messageReport1s) {
        this.baseMapper.insertMessageReports(messageReport1s);
    }
}
