package com.yzq.modular.message.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzq.modular.datasync.entity.DataSync;
import com.yzq.modular.datasync.service.DataSyncService;
import com.yzq.modular.message.entity.MessageReport;
import com.yzq.modular.message.mapper.MessageReportMapper;
import com.yzq.modular.message.service.IMessageReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 周期上报消息记录Service业务层处理
 *
 * @author jihan
 * @date 2024-05-13
 */
@Slf4j
@Service
@DS("master_1")
public class MessageReportServiceImpl extends ServiceImpl<MessageReportMapper, MessageReport> implements IMessageReportService {

    @Resource
    private DataSyncService dataSyncService;
    @Override
    public Integer getTableTotal() {
        List<DataSync> dataSyncs = dataSyncService.list();
        // 只设置一条
        DataSync dataSync = dataSyncs.get(0);

        return this.baseMapper.getTableTotal(dataSync.getStartTime(),dataSync.getEndTime());
    }
}
