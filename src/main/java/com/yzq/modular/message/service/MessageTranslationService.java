package com.yzq.modular.message.service;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: 新老数据转换处理服务类
 * @Date: 2022/4/18 14:03
 */

public interface MessageTranslationService {

    public void translation();
    /**
     * 增量处理
     *
     * @author yzq
     * @date 2024/10/26
     */
    public void incrementalProcessing();
}
