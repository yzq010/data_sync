package com.yzq.modular.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzq.modular.message.entity.MessageReport;
import com.yzq.modular.message.entity.MessageReport1;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 周期上报消息记录Mapper接口
 *
 * @author jihan
 * @date 2024-05-13
 */
@Repository
@Mapper
public interface MessageReport1Mapper extends BaseMapper<MessageReport1> {

    /**
     * 插入消息报告
     *
     * @param messageReport1s 消息报告1s
     * @author yzq
     * @date 2024/10/26
     */
    void insertMessageReports(@Param("paramsList") List<MessageReport1> messageReport1s);
}
