package com.yzq.modular.message.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzq.modular.message.entity.MessageReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 周期上报消息记录Mapper接口
 *
 * @author jihan
 * @date 2024-05-13
 */
@Repository
@Mapper
public interface MessageReportMapper extends BaseMapper<MessageReport> {

    /**
     *
     * 获取旧表的数据总量
     *
     *
     * */
    Integer getTableTotal(@Param("startTime") Date startTime,@Param("endTime") Date endTime);


}
