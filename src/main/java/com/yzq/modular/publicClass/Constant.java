package com.yzq.modular.publicClass;

public interface Constant {
    public static final String MONDAY = "星期一";
    public static final String TUESDAY = "星期二";
    public static final String WEDNESDAY = "星期三";
    public static final String THURSDAY = "星期四";
    public static final String FRIDAY = "星期五";
    public static final String SATURDAY = "星期六";
    public static final String SUNDAY = "星期日";

    public static final String COMPANY = "执行公司";
    public static final String DEPARTMENT = "所属部门";
    public static final String POSITION = "执行职位";
    public static final String TAG = "标签";
    public static final String PROJECT = "所属项目";
    public static final String EXECUTE_PEOPLE = "执行人";
    public static final String PROFESSIONAL_IDENTITY = "职业身份";
    public static final String EXCLUDE_PEOPLE = "排除人员";

    public static final Integer position = 3;
    public static final Integer dept = 2;

}
