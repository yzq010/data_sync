package com.yzq.modular.datasync.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yzq
 * @since 2024-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("data_sync")
public class DataSync implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 开始时间
     */
    @TableField("start_time")
    private Date startTime;

    /**
     * 全量同步截止时间
     */
    @TableField("end_time")
    private Date endTime;


}
