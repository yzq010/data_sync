package com.yzq.modular.datasync.mapper;

import com.yzq.modular.datasync.entity.DataSync;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzq
 * @since 2024-10-26
 */
public interface DataSyncMapper extends BaseMapper<DataSync> {

}
