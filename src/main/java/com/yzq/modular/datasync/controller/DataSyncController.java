package com.yzq.modular.datasync.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yzq
 * @since 2024-10-26
 */
@RestController
@RequestMapping("/dataSync")
public class DataSyncController {

}

