package com.yzq.modular.datasync.service.impl;

import com.yzq.modular.datasync.entity.DataSync;
import com.yzq.modular.datasync.mapper.DataSyncMapper;
import com.yzq.modular.datasync.service.DataSyncService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yzq
 * @since 2024-10-26
 */
@Service
public class DataSyncServiceImpl extends ServiceImpl<DataSyncMapper, DataSync> implements DataSyncService {

}
