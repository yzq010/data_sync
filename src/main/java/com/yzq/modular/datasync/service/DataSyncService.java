package com.yzq.modular.datasync.service;

import com.yzq.modular.datasync.entity.DataSync;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yzq
 * @since 2024-10-26
 */
public interface DataSyncService extends IService<DataSync> {

}
