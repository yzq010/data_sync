package com.yzq.modular.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/16 16:43
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PageInfo /*implements Serializable*/ {

    /*private static final long serialVersionUID = -55L;*/
    /**
     *
     * 每页条数
     * */
    private Integer pageSize;
    /**
     * 总页数
     * */
    private Integer pageNum;
    /**
     * 当前页数
     * */
    private Integer currentPageNum;
    /**
     *
     * 总数
     * */
    private Integer total;
    /**
     *
     * old erp表名
     * */
    private String tableName;

    /**
     *
     * 完成日期
     *
     * */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;
}
