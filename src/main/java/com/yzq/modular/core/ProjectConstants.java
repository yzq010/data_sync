package com.yzq.modular.core;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description:  项目常量
 * @Date: 2022/4/16 19:42
 */
public interface ProjectConstants {

    /**
     *
     * 分页前缀
     * */
    String PAGEINFO_CACHE_PREFIX = "pageinfo:";
    /**
     *
     * 删除状态 Y
     * */
    String STATUS_Y="Y";
    /**
     *
     * 删除状态 N
     *
     * */
    String STATUS_N="N";

}
