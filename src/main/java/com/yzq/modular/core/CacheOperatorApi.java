package com.yzq.modular.core;

import cn.hutool.core.util.StrUtil;

import java.util.Collection;
import java.util.Map;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/16 19:24
 */
public interface CacheOperatorApi<T> {
    void put(String key, T value);

    void put(String key, T value, Long timeoutSeconds);

    T get(String key);

    void remove(String... key);

    void expire(String key, Long expiredSeconds);

    boolean contains(String key);

    Collection<String> getAllKeys();

    Collection<T> getAllValues();

    Map<String, T> getAllKeyValues();

    String getCommonKeyPrefix();

    default String calcKey(String keyParam) {
        return StrUtil.isBlank(keyParam) ? this.getCommonKeyPrefix() : this.getCommonKeyPrefix() + keyParam.toUpperCase();
    }
}
