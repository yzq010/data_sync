package com.yzq.modular.core;

import cn.hutool.core.util.ObjectUtil;

import java.util.List;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: 流程模板
 * @Date: 2022/4/16 18:03
 */
public abstract class CommonService<T> {

    public final  void template(){

         //1. 获取分页信息(先缓存后数据库)
         PageInfo pageInfo = getPageInfo();
         if(ObjectUtil.isNotEmpty(pageInfo)){
             if(ObjectUtil.isNotEmpty(pageInfo.getTotal())
                     && ObjectUtil.isNotEmpty(pageInfo.getPageNum())
                     && ObjectUtil.isNotEmpty(pageInfo.getPageSize())
                     && ObjectUtil.isNotEmpty(pageInfo.getTableName())
                     && ObjectUtil.isNotEmpty(pageInfo.getCurrentPageNum())){
                 int CurrentpageNum = pageInfo.getCurrentPageNum();
                 int pageNum = pageInfo.getPageNum();
                 for(int i = CurrentpageNum ;i<=pageNum;i++){

                     PageInfo currentPageInfo = new PageInfo(
                             pageInfo.getPageSize(),
                             pageNum,
                             i,
                             pageInfo.getTotal(),
                             pageInfo.getTableName(),
                             pageInfo.getDate());
                     /*currentPageInfo.setPageNum(pageNum);
                     currentPageInfo.setPageSize(pageInfo.getPageSize());
                     currentPageInfo.setTotal(pageInfo.getTotal());
                     currentPageInfo.setCurrentPageNum(i);
                     currentPageInfo.setTableName(pageInfo.getTableName());
                     currentPageInfo.setDate(pageInfo.getDate());*/
                     // 2. 写入redis 当前分页信息
                     this.setCache(currentPageInfo);
                     // 3. 分页获取老erp数据
                     List<T>  list = getOldInfo(currentPageInfo);
                     // 4. 数据转换
                     translate(list);

                     if(i==pageNum){
                         // 5. 数据同步完成，将缓存设为完成：currentpageNum=pageNum+1即可，
                         // 每次检查缓存时先判断currentpageNum是否大于pageNum，是，说明数据完成
                         // 否即为未完成
                         this.complateCache(currentPageInfo);
                     }

                 }
             }
         }


    }
    /**
     *
     * 获取分页信息，数据完成时，返回null即可
     * */
    public abstract PageInfo getPageInfo();

    /**
     * 保存当前分页信息
     *
     * */
    public abstract void setCache(PageInfo pageInfo);

    /**
     *
     * 获取缓存的分页信息
     * */
    public abstract PageInfo getCahe(String tableName);
    /**
     *
     * 数据完成同步
     *
     * */
    public abstract void complateCache(PageInfo currentPageInfo);
    /**
     *
     * 获取老rep数据
     *
     * */

    public abstract List<T> getOldInfo(PageInfo pageInfo);
    /**
     *
     * 数据转换接口
     * */
    public abstract void translate(List<T> list);


}
