package com.yzq.modular.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yzq.modular.demo.entity.OldTest;

/**
 *
 * 旧表服务实现类
 *
 *
 * @author yzq
 * @since 2022-04-16
 */
public interface OldTestService extends IService<OldTest> {
    /**
     * 获取旧表数据总量
     *
     * */
    public Integer getTableTotal();
}
