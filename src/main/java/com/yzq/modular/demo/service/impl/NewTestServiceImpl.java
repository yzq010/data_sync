package com.yzq.modular.demo.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzq.modular.demo.entity.NewTest;
import com.yzq.modular.demo.mapper.NewTestMapper;
import com.yzq.modular.demo.service.NewTestService;
import org.springframework.stereotype.Service;
/**
 *
 * 新表服务实现类
 *
 *
 * @author yzq
 * @since 2022-04-16
 */

@Service
@DS("master_1")
public class NewTestServiceImpl extends ServiceImpl<NewTestMapper, NewTest> implements NewTestService {
}
