package com.yzq.modular.demo.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzq.modular.demo.entity.OldTest;
import com.yzq.modular.demo.mapper.OldTestMapper;
import com.yzq.modular.demo.service.OldTestService;
import org.springframework.stereotype.Service;

/**
 *
 * 旧表服务实现类
 *
 *
 * @author yzq
 * @since 2022-04-16
 */

@Service
@DS("master_2")
public class OldTestServiceImpl extends ServiceImpl<OldTestMapper, OldTest> implements OldTestService {
    @Override
    public Integer getTableTotal() {
        return this.baseMapper.getTableTotal();
    }
}
