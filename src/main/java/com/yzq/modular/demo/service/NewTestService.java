package com.yzq.modular.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yzq.modular.demo.entity.NewTest;

/**
 *
 * 新表服务实现类
 *
 *
 * @author yzq
 * @since 2022-04-16
 */
public interface NewTestService extends IService<NewTest> {
}
