package com.yzq.modular.demo.service;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: 新老数据转换处理服务类
 * @Date: 2022/4/18 14:03
 */

public interface TestTranslationService {

    public void translation();
}
