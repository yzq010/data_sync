package com.yzq.modular.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * 新项目表
 *
 *
 * @author yzq
 * @since 2022-04-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_integral")
public class NewTest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 积分
     */
    @TableField("integral_value")
    private Integer integralValue;

    /**
     * 积分类型  3.推荐积分积分 2.项目积分1.学习积分
     */
    @TableField("integral_type")
    private Integer integralType;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}
