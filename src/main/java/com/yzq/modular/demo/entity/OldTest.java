package com.yzq.modular.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *  老数据库表
 *
 * @author yzq
 * */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_integral")
public class OldTest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 学习积分
     */
    @TableField("study_integral")
    private Integer studyIntegral;

    /**
     * 项目积分
     */
    @TableField("project_integral")
    private Integer projectIntegral;

    /**
     * 推荐积分
     */
    @TableField("recommend_integral")
    private Integer recommendIntegral;

    /**
     *  创建时间
     *
     * */
    @TableField("create_time")
    private Long createTime;

}
