package com.yzq.modular.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzq.modular.demo.entity.OldTest;

/**
 *
 * 旧表 oldTest Mapper 接口
 *
 *
 * @author yzq
 * @since 2022-12-27
 */
public interface OldTestMapper extends BaseMapper<OldTest> {
    /**
     *
     * 获取旧表的数据总量
     *
     *
     * */
    Integer getTableTotal();
}
