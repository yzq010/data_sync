package com.yzq.modular.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzq.modular.demo.entity.NewTest;


/**
 *
 * 新表 oldTest Mapper 接口
 *
 *
 * @author yzq
 * @since 2022-12-27
 */
public interface NewTestMapper extends BaseMapper<NewTest> {


}
