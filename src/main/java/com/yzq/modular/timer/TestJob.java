package com.yzq.modular.timer;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.yzq.modular.demo.service.TestTranslationService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/18 10:40
 */
@Component
public class TestJob {
    @Resource
    private TestTranslationService testTranslationService;
    /**
     *
     * old-erp 地区同步
     *
     * */
    @XxlJob("testHandler")
    public ReturnT<String> regionHandler(String param) throws Exception {

        XxlJobHelper.log("XXL-JOB, testHandler begin");
        testTranslationService.translation();
        return ReturnT.SUCCESS;
    }
}
