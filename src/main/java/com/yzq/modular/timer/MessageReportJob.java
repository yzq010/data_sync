package com.yzq.modular.timer;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.yzq.modular.demo.service.TestTranslationService;
import com.yzq.modular.message.service.MessageTranslationService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class MessageReportJob {
    @Resource
    private MessageTranslationService messageTranslationService;
    /**
     *
     * old-erp 地区同步
     *
     * */
    @XxlJob("messageHandler")
    public ReturnT<String> messageHandler(String param) throws Exception {

        XxlJobHelper.log("XXL-JOB, messageHandler begin");
        messageTranslationService.translation();
        return ReturnT.SUCCESS;
    }

    /**
     * 增量处理
     *
     * @param param param
     * @return {@link ReturnT }<{@link String }>
     * @author yzq
     * @date 2024/10/26
     */
    @XxlJob("incrementalProcessing")
    public ReturnT<String> incrementalProcessing(String param) throws Exception {

        XxlJobHelper.log("XXL-JOB, incrementalProcessing begin");
        messageTranslationService.incrementalProcessing();
        return ReturnT.SUCCESS;
    }
}
