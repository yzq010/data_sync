package com.yzq.modular.config;

import com.yzq.modular.cache.PageInfoCache;
import com.yzq.modular.core.PageInfo;
import com.yzq.modular.core.CacheOperatorApi;
import com.yzq.modular.util.CreateRedisTemplateUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/16 19:46
 */
@Configuration
public class RedisCacheConfig {


    @Bean("pageInfoCache")
    public CacheOperatorApi<PageInfo> pageInfoCache(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String, PageInfo> redisTemplate = CreateRedisTemplateUtil.createObject(redisConnectionFactory);
        return new PageInfoCache(redisTemplate);
    }
}
