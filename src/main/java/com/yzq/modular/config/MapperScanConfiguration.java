package com.yzq.modular.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/18 10:53
 */
@Configuration
@MapperScan(basePackages ={"com.yzq.modular.**.mapper"})
public class MapperScanConfiguration {
}
