package com.yzq.modular.config;

import com.yzq.modular.util.CreateRedisTemplateUtil;
import com.yzq.modular.util.FastJson2JsonRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @ProjectName: modular
 * @Author: yzq
 * @Description: ${description}
 * @Date: 2022/4/16 16:26
 */
@Configuration
public class RedisCacheAutoConfiguration {
    /**
     * Redis的value序列化器
     *
     * @author yzq
     * @date 2021/1/31 20:44
     */
    @Bean
    public RedisSerializer<?> fastJson2JsonRedisSerializer() {
        return new FastJson2JsonRedisSerializer<>(Object.class);
    }

    /**
     * value是object类型的redis操作类
     *
     * @author yzq
     * @date 2021/1/31 20:45
     */
    @Bean
    public RedisTemplate<String, Object> objectRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        return CreateRedisTemplateUtil.createObject(redisConnectionFactory);
    }

    /**
     * value是string类型的redis操作类
     *
     * @author yzq
     * @date 2021/1/31 20:45
     */
    @Bean
    public RedisTemplate<String, String> getStringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        return CreateRedisTemplateUtil.createString(redisConnectionFactory);

    }

}
