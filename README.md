# 新老项目全量数据同步项目

**项目简介**

该项目为个人开发，完全开源的简易快速数据全量同步项目

**项目应用背景**

* 老项目的重构改造往往会涉及到数据库表结构的变动，当数据迁移时，由于旧数据库表结构与新数据库库表结构不一致，
单纯的依靠各类工具无法再迁移数据。
* 需要代码完成新老数据的处理转换，才能迁移数据
* 需要简化开发人员开发难度，使普通开发人员的工作集中在新老数据的转换上。

## 项目核心功能

1. 整体采用模板模式：
   com.yzq.modular.core.CommonService 模板类
   模板流程包括：
   (1). 获取老项目数据库分页信息(先查缓存后查数据库)
   (2). 将分页信息写入redis
   (3). 根据分页信息获取旧表的数据
   (4). 新旧数据转换保存
   (5). 检查数据是否完成同步迁移，，将缓存设为完成：currentpageNum=pageNum+1即可，
   每次检查缓存时先判断currentpageNum是否大于pageNum，是，说明数据完成
   否即为未完成。
2. 以 redis作为分页数据缓存工具，每次通过检查分页来分批转换保存数据，采用redis目的：
   数据转换在一两张表的情况下，redis体现不了价值，但是一个项目所包含的表及数据量是巨大的
   采用redis而不是数据库存储同步进度可以更有效分批控制同步进度；第二，数据处理往往会由于数据问题
   导致各类问题，可能前几个分页转换保存完成了，但是中间某个分页数据出问题，这个时候可以借助redis管理工具的
   查看是那个分页数据出了问题，手动处理完成后，定时器就会从redis这个页码继续运行。
3. 采用xxl-job 作为每个新老数据的定时任务，当某个任务出问题了，从xxl-admin的控制台可以找到是哪个任务出错，再配合redis
   来定位具体出错分页。xxl-job执行器在timer目录下。
4. 每个新老表转换包含三个service：NewTestService 新表服务类；OldTestService 旧表服务类；
   TestTranslationService 转换类，也是模板类的子类，是整个流程的控制类。
5. 数据源采用的是 
   <dependency>
      <groupId>com.baomidou</groupId>
      <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
      <version>3.4.1</version>
      </dependency>
   推荐新库放到master_1上，在NewTestService和OldTestService 加入 @DS注解，详细信息可以看mybatis的官网。

目前已知问题：在数据不出错的情况下，定时任务会执行到当前任务所有数据同步完。
现在没有把增量更新代码和上来。

如果有疑问或者更好的意见可以给我留言。

源码地址：https://gitee.com/yzq010/data_sync.git 